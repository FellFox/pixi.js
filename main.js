let player;
let app;
let keys = {};
let keysDiv;
let bullets = [];
let bulletSpeed = 10;
let bullet;
let enemies = []
let enemy
let speed = 3

window.onload = function () {
  app = new PIXI.Application({
    width: 540,
    height: 600,
    backgroundColor: "black",
  });
  player = PIXI.Sprite.from("images/player.png");
  
  for (let i = 0; i < 11; i++) {
    enemy = PIXI.Sprite.from("images/enemy.png");
    enemies.push(enemy)
 }

 for (let i = 0; i < enemies.length; i++) {
  enemies[i].anchor.set(1);
  enemies[i].x = 53 *i;
  enemies[i].y = 50
   app.stage.addChild(enemies[i]);
  
}



  player.anchor.set(0.5);
  player.x = app.view.width / 2.5;
  player.y = app.view.height / 1.1;
  app.stage.addChild(player);

  document.querySelector("#gameDiv").appendChild(app.view);
  keysDiv = document.querySelector("#keys");
  app.ticker.add(gameLoop);
  app.stage.interactive = true;
  document
    .querySelector("#gameDiv")
    .addEventListener("pointerdown", fireBullet);

  window.addEventListener("keydown", keysDown);
  window.addEventListener("keyup", keysUp);

  function keysDown(e) {
    keys[e.keyCode] = true;
  }

  function keysUp(e) {
    keys[e.keyCode] = false;
  }

  function gameLoop() {
    
    updatePlayerMovement()
    updateBullets();
   
  }

  function updatePlayerMovement() {
    if (keys["87"]) {
        player.y -= 5;
      }
      if (keys["83"]) {
        player.y += 5;
      }
      if (keys["65"]) {
        player.x -= 5;
      }
      if (keys["68"]) {
        player.x += 5;
      }
  }

  function createBullet() {
    bullet = PIXI.Sprite.from("images/bullet.png");
    bullet.anchor.set(0.5);
    bullet.x = player.x;
    bullet.y = player.y;
    bullet.speed = bulletSpeed;
    app.stage.addChild(bullet);

    return bullet;
  }

  function fireBullet() {
    let bullet = createBullet();
    bullets.push(bullet);
  }

  function updateBullets(delta) {
    for (let i = 0; i < bullets.length; i++) {
      bullets[i].position.y -= bullets[i].speed;
      if(bullets[i].position.y < 0) {
        bullets[i].dead = true;
      }
      if(bulletHit(bullets[i],enemy)){
        app.stage.removeChild(enemy)
        app.stage.removeChild(bullets[i])

    }
    }
    for (let i = 0; i < bullets.length; i++) {
        if(bullets[i].dead){
            app.stage.removeChild(bullets[i])
            bullets.splice(i,1)
        }
    }
   
  }

  function bulletHit(enemy, bullet) {
    let enemyBox = enemy.getBounds()
    let bulletBox = bullet.getBounds()

    return enemyBox.x + enemyBox.width > bulletBox.x &&
            enemyBox.x < bulletBox.x + bulletBox.width &&
            enemyBox.y + enemyBox.height > bulletBox.y &&
            enemyBox.y < bulletBox.y + bulletBox.height 
  }



};
